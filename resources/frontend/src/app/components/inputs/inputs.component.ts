import { Component, Input } from '@angular/core';
import { ApiService, TableData, ApiResponse } from '../../services/api.service';
import { NgForm } from '../../../../node_modules/@angular/forms';
import { TableService } from '../../services/table.service';
import { MatSnackBar } from '../../../../node_modules/@angular/material';

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.css']
})
export class InputsComponent {
  readonly methods = ['DELETE','GET','PATCH','POST','PUT'];
  selectedValue: string;
  numberOfFields:number[] = [];
  historic:TableData[] = [];

  constructor(private apiService : ApiService, private data:TableService,private snackBar:MatSnackBar) { }

  makeRequest(frm:NgForm){
    if(frm.valid){
      let initTime = performance.now();
      this.apiService.makeRequest(this.selectedValue,this.createSendingObject(frm.value))
      .subscribe(((r:ApiResponse)=>{
        let requestTime = performance.now()-initTime;
        let newReg : TableData = {
          position:this.historic.length+1,
          method:r.method,
          fields:r.input,
          duration:requestTime,
          timestamp:new Date()
        }
        let message = "Method:"+r.method+" Fields:"+r.input;
        let action = requestTime.toString()+"ms";
        this.snackBar.open(message,action,{duration:5000});
        this.historic.push(newReg);
        this.data.changeData(this.historic);
      }));
    }
  }

  createSendingObject(obj:Object):Object{
    let ret = new Object();
    this.numberOfFields.forEach(n=>{
      let k = obj["key"+n];
      let v = obj["val"+n];
      ret[k]=v;
    });
    return ret;
  }

  addFields(){
    this.numberOfFields.push(this.numberOfFields.length+1);
  }

  removeFields(){
    this.numberOfFields.pop();
  }
}
