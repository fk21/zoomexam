import { Component, OnInit } from '@angular/core';
import { TableData } from '../../services/api.service';
import { TableService } from '../../services/table.service';
import { MatTableDataSource } from '../../../../node_modules/@angular/material';

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.css']
})
export class HistoricComponent implements OnInit {
  historic = new MatTableDataSource<TableData>();
  readonly displayedColumns = ['position', 'fields', 'method', 'duration','timestamp'];
  constructor(private data: TableService) { }

  ngOnInit(){
    this.data.currentData.subscribe(data =>{
      this.historic.data = data;
    });
  }

}
