import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface ApiResponse {
    method:string;
    input: string;
}
export interface TableData {
  position:number;
  method:string;
  fields:string;
  duration:number;
  timestamp:Date;
}
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  readonly endpoint = '/api/endpoint';

  constructor(private http:HttpClient) {
  }

  public makeRequest(type:string,inputs:Object){
    switch(type){
      case 'GET':
      return this.getRequest(this.makeGetQuery(inputs));
      case 'POST':
      return this.postRequest(inputs);
      case 'PUT':
      return this.putRequest(inputs);
      case 'PATCH':
      return this.patchRequest(inputs);
      case 'DELETE':
      return this.deleteRequest(this.makeGetQuery(inputs));
      default:
      break;
    }
  }

  private makeGetQuery(inputs:Object):string{
    let keys = Object.keys(inputs);
    let query = '?';
    for(let i = 0; i<keys.length; i++){
      if(i == keys.length -1){
        query+=i.toString()+'['+keys[i]+']='+inputs[keys[i]];
      }
      else{
        query+=i.toString()+'['+keys[i]+']='+inputs[keys[i]]+'&';
      }
    }
    return query;
  }

  private getRequest(query:string){
    let url = this.endpoint+query;
    const headers = new HttpHeaders({
      'Content-Type':'application/json'
    });
    return this.http.get(url,{headers});
  }

  private postRequest(inputs:Object){
    const headers = new HttpHeaders({
      'Content-Type':'application/json'
    });
    return this.http.post(this.endpoint,inputs,{headers});
  }

  private patchRequest(inputs:Object){
    const headers = new HttpHeaders({
      'Content-Type':'application/json'
    });
    return this.http.patch(this.endpoint,inputs,{headers});
  }

  private deleteRequest(query:string){
    let url = this.endpoint+query;
    const headers = new HttpHeaders({
      'Content-Type':'application/json'
    });
    return this.http.delete(url,{headers});
  }

  private putRequest(inputs:Object){
    const headers = new HttpHeaders({
      'Content-Type':'application/json'
    });
    return this.http.put(this.endpoint,inputs,{headers});
  }


}
