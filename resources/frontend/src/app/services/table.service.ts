import { Injectable } from '@angular/core';
import { TableData } from './api.service';
import { BehaviorSubject } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  private dataSource = new BehaviorSubject<TableData[]>([]);
  currentData = this.dataSource.asObservable();
  constructor() { }

  changeData(newData : TableData[]){
    this.dataSource.next(newData);
  }
}
