<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('endpoint','ApiController@endpoint');
Route::put('endpoint','ApiController@endpoint');
Route::delete('endpoint','ApiController@endpoint');
Route::patch('endpoint','ApiController@endpoint');
Route::get('endpoint','ApiController@endpoint');
