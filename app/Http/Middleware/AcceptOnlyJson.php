<?php

namespace App\Http\Middleware;

use Closure;

class AcceptOnlyJson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->isJson()){
            return $next($request);
        }
        return response()->json([
            'error'=>'This endpoint only accept JSON requests'
        ],400);
    }
}
