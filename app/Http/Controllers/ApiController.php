<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ApiController extends Controller
{

    public function endpoint(Request $request){
        $input = json_encode($request->all());
        $method = $request->method();
        return compact('method','input');
    }

}
