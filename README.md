# ZoomCatalog Full Stack Exam

### The Exam
#### Instructions

**Echo server**
Write an application program interface for HTTP requests that could handle POST, PUT, DELETE, PATCH and GET requests (A kind of an echo server), the response of each request must be a valid JSON following this structure: 
    
    {
    “method”: <HTTP METHOD REQUESTED>,
    “data”: <HTTP PARAMETERS SENT IN THE REQUEST>
    }

#### Considerations:
- You must use the endpoint /data
- Only should allow JSON parameters 
- Use the best practices of coding to create the exercise


**Example:**
> Request: curl -d '{"key1":"value1", "key2":"value2"}' -X POST http://localhost:3000/data
> Response: {"method": "POST", "data": {"key1":"value1", "key2":"value2"}}

**UI Client**
Create a very simple Angular 2+ user interface to execute the last exercise, basically need a way to choose the HTTP method, something to send data and a button to send the request; The JSON response needs to be displayed and also the time in seconds the request takes to be completed. 


### The solution

  - I choose to use PHP (7.2+) as a programming language.
  - I use Angular 7 for the frontend.
  - I use Laravel Framework
  - The frontend app is in ./resources/frontend
### Running the Project
- Run composer install command
- Change the extension of the file *.env.example* to **.env** (Environment Variables)
- Run php artisan key:generate (Encryption Key)
- Run php artisan serve and that's all or alternatively use Valet

### Running as a Developer
The application is ready to run with *ng serve* and *artisan serve* at the same time, the backend app accept Cross Origin Request just for this exam, so feel free to change the code and run any test.

License
----

MIT
